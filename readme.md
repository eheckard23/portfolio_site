![logo.png](https://bitbucket.org/repo/5KLbee/images/2079112299-logo.png)
#Ethan Heckard Portfolio
I created this repository to show the code underneath my latest portfolio site. After spending a couple weeks designing the site in Photoshop, I moved on to creating the markup. The site was created using Bootstrap as a means for responsive design, not the overall appearance.